package com.example.notlarappretrofit

import android.content.Intent
import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.notlarappretrofit.databinding.ActivityMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var notlarListe:ArrayList<Notlar>
    private lateinit var adapter:NotlarAdapter
    private lateinit var ndi:NotlarDaoInterface
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding.toolbarMain.title = "Notlar uygulaması"

        setSupportActionBar(binding.toolbarMain)

        binding.rv.setHasFixedSize(true)
        binding.rv.layoutManager = LinearLayoutManager(this)
        ndi = ApiUtils.getNotlarDaoInterface()

        tumNotlar()


        binding.fab.setOnClickListener{
            startActivity(Intent(this@MainActivity,NotKayit::class.java))
        }

    }

    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        super.onBackPressed()
    }
    fun tumNotlar(){
        ndi.tumNotlar().enqueue(object : Callback<NotlarCevap>{
            override fun onResponse(call: Call<NotlarCevap>, response: Response<NotlarCevap>) {
                if (response != null){
                    val liste = response.body()?.notlar
                    if (liste != null) {
                        adapter = NotlarAdapter(this@MainActivity, liste!!)
                        binding.rv.adapter = adapter
                        var toplam = 0
                        for (n in liste) {
                            toplam = toplam + (n.not1 + n.not2) / 2
                        }
                        binding.toolbarMain.subtitle = "Ortalama : ${toplam / liste.size}"
                    }
                }
            }
            override fun onFailure(p0: Call<NotlarCevap>, p1: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }
}