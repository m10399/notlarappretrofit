package com.example.notlarappretrofit

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.notlarappretrofit.databinding.ActivityNotKayitBinding
import com.google.android.material.snackbar.Snackbar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotKayit : AppCompatActivity() {
    private lateinit var binding: ActivityNotKayitBinding
    private lateinit var ndi : NotlarDaoInterface
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityNotKayitBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding.toolbarNotKayit.title = "Not Kayıt"
        setSupportActionBar(binding.toolbarNotKayit)
        ndi = ApiUtils.getNotlarDaoInterface()
        binding.buttonNotKayit.setOnClickListener{
            val ders_adi = binding.editTextDersAdi.text.toString().trim()
            val not1 = binding.editTextTextNot1.text.toString().trim()
            val not2 = binding.editTextTextNot2.text.toString().trim()
            if (TextUtils.isEmpty(ders_adi)){
                Snackbar.make(binding.toolbarNotKayit,"Ders adını giriniz",Snackbar.LENGTH_SHORT).show()
            }
            if (TextUtils.isEmpty(not1)){
                Snackbar.make(binding.toolbarNotKayit,"1. notu giriniz",Snackbar.LENGTH_SHORT).show()
            }
            if (TextUtils.isEmpty(not2)){
                Snackbar.make(binding.toolbarNotKayit,"2. notu giriniz",Snackbar.LENGTH_SHORT).show()
            }
            ndi.notEkle(ders_adi,not1.toInt(),not2.toInt()).enqueue(object : Callback<CRUDCevap>{
                override fun onResponse(p0: Call<CRUDCevap>, p1: Response<CRUDCevap>) {

                }

                override fun onFailure(p0: Call<CRUDCevap>, p1: Throwable) {

                }

            })
            startActivity(Intent(this@NotKayit,MainActivity::class.java))
            finish()
        }

    }
}