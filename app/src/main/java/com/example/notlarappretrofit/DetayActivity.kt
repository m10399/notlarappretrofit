package com.example.notlarappretrofit

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.notlarappretrofit.databinding.ActivityDetayBinding
import com.google.android.material.snackbar.Snackbar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetayActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetayBinding
    private lateinit var ndi:NotlarDaoInterface
    private lateinit var not:Notlar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityDetayBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding.toolbarNotDetay.title = "Not Detay"
        setSupportActionBar(binding.toolbarNotDetay)
        ndi = ApiUtils.getNotlarDaoInterface()

        not = intent.getSerializableExtra("nesne") as Notlar
        binding.editTextDersAdi.setText(not.ders_adi)
        binding.editTextTextNot1.setText(not.not1.toString())
        binding.editTextTextNot2.setText(not.not2.toString())

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_sil ->{
                Snackbar.make(binding.root,"Silinsin mi?",Snackbar.LENGTH_LONG).setAction("evet"){
                    ndi.notSil(not.not_id).enqueue(object: Callback<CRUDCevap>{
                        override fun onResponse(p0: Call<CRUDCevap>, p1: Response<CRUDCevap>) {

                        }

                        override fun onFailure(p0: Call<CRUDCevap>, p1: Throwable) {

                        }

                    })
                    startActivity(Intent(this@DetayActivity,MainActivity::class.java))
                    finish()
                }.show()
                return true
            }
            R.id.action_duzenle->{
                val ders_adi = binding.editTextDersAdi.text.toString().trim()
                val not1 = binding.editTextTextNot1.text.toString().trim()
                val not2 = binding.editTextTextNot2.text.toString().trim()
                if (TextUtils.isEmpty(ders_adi)){
                    Snackbar.make(binding.toolbarNotDetay,"Ders adını giriniz",Snackbar.LENGTH_SHORT).show()
                }
                if (TextUtils.isEmpty(not1)){
                    Snackbar.make(binding.toolbarNotDetay,"1. notu giriniz",Snackbar.LENGTH_SHORT).show()
                }
                if (TextUtils.isEmpty(not2)){
                    Snackbar.make(binding.toolbarNotDetay,"2. notu giriniz",Snackbar.LENGTH_SHORT).show()
                }
                ndi.notGuncelle(not.not_id,ders_adi,not1.toInt(),not2.toInt()).enqueue(object : Callback<CRUDCevap>{
                    override fun onResponse(p0: Call<CRUDCevap>, p1: Response<CRUDCevap>) {
                        
                    }

                    override fun onFailure(p0: Call<CRUDCevap>, p1: Throwable) {

                    }

                })
                startActivity(Intent(this@DetayActivity,MainActivity::class.java))
                finish()
                return true
            }
            else-> return false

        }
        return super.onOptionsItemSelected(item)

    }
}